package com.example.test.example.controller;

import com.example.test.example.dto.LoginDTO;
import com.example.test.example.dto.RegisterDTO;
import com.example.test.example.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/service")
public class ServiceController {

    @Autowired
    private UsersService usersService;

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerUser(@RequestBody RegisterDTO registerDTO) {

        try {
           registerDTO = usersService.register(registerDTO);
            return ResponseEntity.ok(registerDTO);
        } catch (RuntimeException r) {
            r.printStackTrace();
            return new ResponseEntity<>("Error! " + r.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity loginUser(@RequestBody LoginDTO loginDTO) {

        try {
            loginDTO = usersService.login(loginDTO);
            return ResponseEntity.ok(loginDTO);
        } catch (RuntimeException r) {
            r.printStackTrace();
            return new ResponseEntity<>("Unauthorized!  " + r.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

}
