package com.example.test.example.controller;

import com.example.test.example.dto.ProgramDTO;
import com.example.test.example.service.ProgammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/content/programs")
public class ProgramController {

    @Autowired
    private ProgammeService progammeService;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createProgramme(@RequestBody ProgramDTO programDTO) {
        try {
            programDTO = progammeService.create(programDTO);
            return ResponseEntity.ok(programDTO);
        } catch (RuntimeException r) {
            r.printStackTrace();
            return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateProgramme (@RequestBody ProgramDTO programDTO) {
        try {
            programDTO = progammeService.update(programDTO);
            return ResponseEntity.ok(programDTO);
        } catch (RuntimeException r) {
            r.printStackTrace();
            return new ResponseEntity<>("Error! " + r.getMessage() ,HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll (@RequestParam("offset")Long offset , @RequestParam("start_date")String start_date,
                                  @RequestParam("end_date")String end_date, @RequestParam("limit")Long limit) {
        List<ProgramDTO> programDTOS = progammeService.getAll(offset, start_date, end_date, limit);
        return ResponseEntity.ok(programDTOS);
    }

}
