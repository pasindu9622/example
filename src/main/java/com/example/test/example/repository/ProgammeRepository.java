package com.example.test.example.repository;

import com.example.test.example.entity.Progamme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProgammeRepository extends JpaRepository<Progamme, Long> {



}
