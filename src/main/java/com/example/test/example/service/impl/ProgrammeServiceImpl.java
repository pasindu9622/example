package com.example.test.example.service.impl;

import com.example.test.example.dto.ProgramDTO;
import com.example.test.example.entity.Progamme;
import com.example.test.example.repository.ProgammeRepository;
import com.example.test.example.service.ProgammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProgrammeServiceImpl implements ProgammeService {

    @Autowired
    private ProgammeRepository progammeRepository;

    @Override
    public ProgramDTO create(ProgramDTO programDTO) {
        Progamme programme = new Progamme();
        programme.setDescription(programDTO.getDescription());
        programme.setEnd_date(programDTO.getEnd_date());
        programme.setName(programDTO.getName());
        programme.setStart_date(programDTO.getStart_date());

        programme = progammeRepository.save(programme);
        return programDTO;
    }

    @Override
    public ProgramDTO update(ProgramDTO programDTO) {
        Optional<Progamme> optionalProgamme = progammeRepository.findById(Long.parseLong(programDTO.getProgram_id()));
        if (!optionalProgamme.isPresent()) {
            throw new RuntimeException("No programme!");
        }
        Progamme programme = optionalProgamme.get();
        programme.setDescription(programDTO.getDescription());
        programme.setEnd_date(programDTO.getEnd_date());
        programme.setName(programDTO.getName());
        programme.setStart_date(programDTO.getStart_date());

        programme = progammeRepository.save(programme);
        return programDTO;
    }

    @Override
    public List<ProgramDTO> getAll(Long offset ,String start_date, String end_date, Long limit) {
        ArrayList<ProgramDTO> programDTOS = new ArrayList<>();
        List<Progamme> all = progammeRepository.findAll();
        all.forEach(programEntity -> {
            ProgramDTO programme = new ProgramDTO();
            programme.setDescription(programEntity.getDescription());
            programme.setEnd_date(programEntity.getEnd_date());
            programme.setName(programEntity.getName());
            programme.setStart_date(programEntity.getStart_date());
            programme.setProgram_id(String.valueOf(programEntity.getProgrammeId()));
            if (Date.valueOf(programEntity.getStart_date()).after(Date.valueOf(start_date))
                    && Date.valueOf(programEntity.getEnd_date()).before(Date.valueOf(end_date))) {
                programDTOS.add(programme);
            }
        });
        return programDTOS;
    }
}
