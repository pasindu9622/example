package com.example.test.example.service;

import com.example.test.example.dto.ProgramDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProgammeService {

    ProgramDTO create(ProgramDTO programDTO);
    ProgramDTO update(ProgramDTO programDTO);
    List<ProgramDTO> getAll(Long offset ,String start_date, String end_date, Long limit);
}
