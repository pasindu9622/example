package com.example.test.example.service;

import com.example.test.example.dto.LoginDTO;
import com.example.test.example.dto.RegisterDTO;
import com.example.test.example.entity.Users;
import org.springframework.stereotype.Service;


@Service
public interface UsersService {

    RegisterDTO register(RegisterDTO registerDTO);

    LoginDTO login(LoginDTO loginDTO);
}
