package com.example.test.example.service.impl;

import com.example.test.example.config.security.JWTAuthenticator;
import com.example.test.example.dto.LoginDTO;
import com.example.test.example.dto.RegisterDTO;
import com.example.test.example.entity.Users;
import com.example.test.example.repository.UserRepository;
import com.example.test.example.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.test.example.config.security.JWTConstants.ISSUER;
import static com.example.test.example.config.security.JWTConstants.SUBJECT;


@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public RegisterDTO register(RegisterDTO registerDTO) {
        Users byUsername = userRepository.findByUsername(registerDTO.getUsername());
        if (byUsername != null) {
            throw new RuntimeException("Username already exists !");
        }
        Users users = new Users();
        users.setName(registerDTO.getName());
        users.setDate_of_birth(registerDTO.getDate_of_birth());
        users.setGender(registerDTO.getGender());
        users.setLanguage(registerDTO.getLanguage());
        users.setMobile_number(registerDTO.getMobile_number());
        users.setPassword(registerDTO.getPassword());
        users.setUsername(registerDTO.getUsername());

        users = userRepository.save(users);
        return registerDTO;
    }

    @Override
    public LoginDTO login(LoginDTO loginDTO) {
        Users byUsername = userRepository.findByUsername(loginDTO.getUsername());
        if (byUsername == null) {
            throw new RuntimeException("No user found!");
        }
        if (!byUsername.getPassword().equals(loginDTO.getPassword())) {
            throw new RuntimeException("Invalid password");
        }
        String jwt = JWTAuthenticator.createJWT(loginDTO.getUsername(), ISSUER, SUBJECT, 86400000);
        loginDTO.setToken(jwt);
        return loginDTO;
    }
}
